kernel void Kernel(__global unsigned char* in, __global double* out, double mean, double variance, int l, unsigned int t)
{
	 int j = get_global_id(0);

	unsigned int r = 0, w = 0, mod8C = 0, shC = 0, tmp = 0, buffC = 0;
	bool startFlag = false;
	for (int i = 3; i >= 0; i--)
	{
		tmp = in[j*l + buffC];
		buffC++;
		tmp <<= 8 * i;
		r |= tmp;
	}
	while (buffC < l)
	{
		
		if ((r & 0xff000000) == t)
		{
			w++;
			shC = 8;
		}
		else shC = 1;
		while (shC > 0)
		{
			
			if (mod8C % 8 == 0 && startFlag)
			{
				tmp = 0;
				tmp = in[j*l + buffC];
				buffC++;
				r = r | tmp;
			}
			r = r << 1;
			mod8C++;
			shC--;
		}
		startFlag = true;
	}
	for (int i = 0; i <= 23; i++)
	{
		if ((r & 0xff000000) == t)
		{
			w++;
			r <<= 8;
			i += 7;
		}
		r <<= 1;
	}
	out[j] = ((w - mean)*(w - mean)) / variance;
	//printf("out[%d] = %g",j,out[j]);
}