#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CL/cl.h>
#include "Struktury.h"
#include "SpecFuncCephes.h"
#include <time.h>



int main(int argsnr, char **argtab)
{
	time_t start, finish;
	start = clock();
	cl_int error = 0;
	if (argsnr != 3)
	{
		printf("ERROR: Bledna liczba parametrow!\n");
		return error;
	}
	FILE *file=NULL;
	file = fopen(argtab[1], "rb");
	if (file == NULL)
	{
		printf("ERROR: Plik o nazwie %s nie istnieje!\n",argtab[1]);
		return error;
	}
	unsigned int t=0x00000000,tmp=0x80000000;
	for (int l = 0; (argtab[2][l]!='\0'); l++)
	{
		if (argtab[2][l] == '1')
		{
			tmp >>= l;
			t = t | tmp;
			tmp = 0x80000000;
		}
		if (l > 7)
		{
			printf("ERROR: Parametr %s jest za dlugi!\n", argtab[2]);
			return error;
		}
	}
	printf("Zadany wzor testowy = %s\n", argtab[2]);
	fseek(file, 0, SEEK_END);
	unsigned int fileSize = ftell(file);
	if (fileSize <= 0)
	{
		printf("ERROR: Plik o nazwie %s jest pusty!\n", argtab[1]);
		return error;
	}
	printf("Rozmiar pliku wejsciowego = %d[Bajtow]\nWykryte platformy i urzadzenia:\n", fileSize);
	unsigned char *fileBuffor = NULL;
	fileBuffor = (unsigned char*)malloc(fileSize*sizeof(unsigned char));
	if (fileBuffor == NULL)
	{
		printf("Nie udalo sie zarezerwowac pamieci!\n");
		return 0;
	}
	fseek(file, 0, SEEK_SET);
	if (fread(fileBuffor,sizeof(unsigned char),fileSize,file)!= fileSize)
	{
		printf("Nie udalo sie przeczytac pliku!\n");
		return 101;
	}
	fclose(file);


	// Platform number.
	cl_uint platformNumber = 0;
	// Platform identifiers.
	cl_platform_id* platformIds = NULL;
	// Device count.
	cl_uint deviceNumber = NULL;
	platforma *platformy = NULL;
	int work_items = 0;

	error = clGetPlatformIDs(0, NULL, &platformNumber);
	if (error != CL_SUCCESS) {
		printf("ERROR: Nie ma platform!\n");
		return error;
	}
	platformy = (platforma *)malloc(sizeof(platforma)*platformNumber);
	platformIds = (cl_platform_id*)malloc(sizeof(cl_platform_id) * platformNumber);

	error = clGetPlatformIDs(platformNumber, platformIds, NULL);
	if (error != CL_SUCCESS) {
		printf("ERROR: Nie udalo sie pobrac platform!\n");
		return error;
	}
	// Get platform info.
	for (cl_uint i = 0; i < platformNumber; ++i)
	{
		char name[1024] = { '\0' };

		printf("Platform:\t%d\n", i);

		platformy[i].platform_id = i;

		error = clGetPlatformInfo(platformIds[i], CL_PLATFORM_NAME, 1024, &name, NULL);

		printf("Name:\t\t%s\n", name);

		strcpy(platformy[i].name, name);

		error = clGetPlatformInfo(platformIds[i], CL_PLATFORM_VENDOR, 1024, &name, NULL);

		printf("Vendor:\t\t%s\n", name);

		strcpy(platformy[i].vendor, name);

		error = clGetDeviceIDs(platformIds[i], CL_DEVICE_TYPE_GPU | CL_DEVICE_TYPE_CPU, 0, NULL, &deviceNumber);
		if (error != CL_SUCCESS) {
			printf("ERROR: Nie uda�o si� pobra� urz�dzenia!\n");
			return error;
		}
		platformy[i].platform_device = (device*)malloc(sizeof(device)*deviceNumber);
		platformy[i].device_number = deviceNumber;
		// Get device identifiers.
		platformy[i].deviceIds = (cl_device_id*)malloc(sizeof(cl_device_id)*deviceNumber);

		error = clGetDeviceIDs(platformIds[i], CL_DEVICE_TYPE_GPU | CL_DEVICE_TYPE_CPU, deviceNumber, platformy[i].deviceIds, &deviceNumber);

		// Get device info.
		for (cl_uint j = 0; j < deviceNumber; ++j)
		{
			char name[1024] = { '\0' };
			size_t group_size = NULL;
			cl_ulong memSize = 0;

			printf("Device:\t\t%d\n", j);

			platformy[i].platform_device[j].device_id = j;

			error = clGetDeviceInfo(platformy[i].deviceIds[j], CL_DEVICE_NAME, 1024, &name, NULL);

			printf("Name:\t\t%s\n", name);

			strcpy(platformy[i].platform_device[j].name,name);
			
			error = clGetDeviceInfo(platformy[i].deviceIds[j], CL_DEVICE_VENDOR, 1024, &name, NULL);

			printf("Vendor:\t\t%s\n", name);

			strcpy(platformy[i].platform_device[j].vendor, name);

			error = clGetDeviceInfo(platformy[i].deviceIds[j], CL_DEVICE_VERSION, 1024, &name, NULL);

			printf("Version:\t%s\n", name);

			strcpy(platformy[i].platform_device[j].version, name);

			error = clGetDeviceInfo(platformy[i].deviceIds[i], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), (void*)&group_size, NULL);

			printf("Max group size:\t%d\n", group_size);

			error = clGetDeviceInfo(platformy[i].deviceIds[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), (void*)&memSize, NULL);

			platformy[i].platform_device[j].maxDeviceMemSize = memSize;
			platformy[i].platform_device[j].max_work_group = group_size;
			
			platformy[i].platform_device[j].context = NULL;
			platformy[i].platform_device[j].queue = NULL;

		}
		printf("\n");
		
	}

	//ustalenie rozmiaru pojedynczego work-itema
	int allWorkItems = 0;
	for (int i = 0; i < platformNumber; i++)
		for (int j = 0; j < platformy[i].device_number; j++)
			allWorkItems += platformy[i].platform_device[j].max_work_group;

	int workItemSize = 0, devideCheck = 0;
	devideCheck = fileSize % allWorkItems;
	workItemSize = fileSize / allWorkItems;

	for (int i = 0; i < platformNumber; i++)
		for (int j = 0; j < platformy[i].device_number; j++)
				platformy[i].platform_device[j].bufforSize = workItemSize*platformy[i].platform_device[j].max_work_group;
	platformy[platformNumber-1].platform_device[platformy[platformNumber-1].device_number-1].bufforSize += devideCheck;

	//uruchomienie kontekstow,kolejek,buforow,kreneli
	int counter = 0;
	double mean = ((double)(8*workItemSize) - 7.0) / 256.0, variance = (double)(8*workItemSize)*((1.0 / 256.0) - (15.0/65536.0));
	file = fopen("Kernel.cl", "r");
	if (file == NULL)
	{
		printf("ERROR: Nie udalo sie otworzyc pliku kernel'a!\n");
		return error;
	}
	fseek(file, 0, SEEK_END);
	size_t kernelSize = ftell(file);
	fseek(file, 0, SEEK_SET);
	char *kernelText = (char*)malloc(kernelSize*sizeof(char));
	for (char byte = 0; fread(&byte, sizeof(char), 1, file) == 1; counter++)
	{
		if (byte == '\n')
		{
			kernelText[counter] = '\r';
			counter++;
		}
		kernelText[counter] = byte;
	}
	if (counter != kernelSize)
	{
		printf("Nie udalo sie przeczytac pliku kernel'a!\n");
		return 102;
	}
	counter = 0;
	for (int i = 0; i < platformNumber; i++)
	{
		for (int j = 0; j < platformy[i].device_number; j++)
		{

			platformy[i].platform_device[j].context = clCreateContext(NULL, 1, &(platformy[i].deviceIds[j]), NULL, NULL, &error);
			if (error != CL_SUCCESS) 
			{
				printf("ERROR: Nie uda�o si� utworzy� kontekstu!\n");
				return error;
			}
			platformy[i].platform_device[j].queue = clCreateCommandQueue(platformy[i].platform_device[j].context,platformy[i].deviceIds[j], NULL, &error);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� utworzy� kolejki!\n");
				return error;
			}

			platformy[i].platform_device[j].bufforIn = clCreateBuffer(platformy[i].platform_device[j].context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, platformy[i].platform_device[j].bufforSize, fileBuffor+counter, &error);
			counter += platformy[i].platform_device[j].bufforSize;
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� utworzy� bufora wejscia dla urzadzenia o numerze id %d!\n",j);
				return error;
			}

			platformy[i].platform_device[j].bufforOUT = (double*)malloc(platformy[i].platform_device[j].max_work_group*sizeof(double));
			platformy[i].platform_device[j].bufforOut = clCreateBuffer(platformy[i].platform_device[j].context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, platformy[i].platform_device[j].max_work_group*sizeof(double), platformy[i].platform_device[j].bufforOUT, &error);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� utworzy� bufora wyjscia dla urzadzenia o numerze id %d!\n", j);
				return error;
			}

			platformy[i].platform_device[j].program = clCreateProgramWithSource(platformy[i].platform_device[j].context, 1, (const char**)&kernelText, &kernelSize, &error);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� utworzy� programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}

			error = clBuildProgram(platformy[i].platform_device[j].program, 1, &(platformy[i].deviceIds[j]), NULL, NULL, NULL);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� zbudowac programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			platformy[i].platform_device[j].kernel = clCreateKernel(platformy[i].platform_device[j].program, "Kernel", &error);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� pobrac kernela z programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			error = clSetKernelArg(platformy[i].platform_device[j].kernel, 0, sizeof(cl_mem), &platformy[i].platform_device[j].bufforIn);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� wyslac pierwszego argumentu do kernela z programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			error = clSetKernelArg(platformy[i].platform_device[j].kernel, 1, sizeof(cl_mem), &platformy[i].platform_device[j].bufforOut);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� wyslac drugiego argumentu do kernela z programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			error = clSetKernelArg(platformy[i].platform_device[j].kernel, 2, sizeof(double), (void*)&mean);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� wyslac trzeciego argumentu do kernela z programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			error = clSetKernelArg(platformy[i].platform_device[j].kernel, 3, sizeof(double), (void*)&variance);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� wyslac czwartego argumentu do kernela z programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			error = clSetKernelArg(platformy[i].platform_device[j].kernel, 4, sizeof(int), (void*)&workItemSize);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� wyslac piatego argumentu do kernela z programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			error = clSetKernelArg(platformy[i].platform_device[j].kernel, 5, sizeof(unsigned int), (void*)&t);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� wyslac szostego argumentu do kernela z programu dla urzadzenia o numerze id %d!\n", j);
				return error;
			}
			error = clEnqueueNDRangeKernel(platformy[i].platform_device[j].queue, platformy[i].platform_device[j].kernel, 1, NULL, &(platformy[i].platform_device[j].max_work_group), &(platformy[i].platform_device[j].max_work_group), 0, NULL, NULL);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Nie uda�o si� uruchomic kernela na urzadzenia o numerze id %d!\n", j);
				return error;
			}
		}
	}

	double chiStat = 0;
	for (int i = 0; i < platformNumber; i++)
	{
		for (int j = 0; j < platformy[i].device_number; j++)
		{
			error = clEnqueueReadBuffer(platformy[i].platform_device[j].queue, platformy[i].platform_device[j].bufforOut, CL_TRUE, 0, sizeof(double)*platformy[i].platform_device[j].max_work_group, platformy[i].platform_device[j].bufforOUT, 0, NULL, NULL);
			if (error != CL_SUCCESS)
			{
				printf("ERROR: Program kernel'a na urzadzeniu o numerze id %d nie wykonal sie poprawnie!\n", j);
				return error;
			}
			for (int k = 0; k < platformy[i].platform_device[j].max_work_group; k++)
				chiStat += platformy[i].platform_device[j].bufforOUT[k];
		}
	}
	double pValue = igamc(((double)allWorkItems/2),((double)chiStat/2));
	finish = clock();
	printf("Czas wykonania programu = %f[s]"
			"\nP-value = %f\n", (finish - start)/(double)CLOCKS_PER_SEC, pValue);
	if (pValue < 0.01)
	{
		printf("P-value<0.01, wiec badany ciag nie jest (pseudo)losowy\n");
	}
	else
	{
		printf("P-value>=0.01, wiec badany ciag jest (pseudo)losowy\n");
	}


	// Free memory.
	free(platformIds);

	free(fileBuffor);
	// Free memory!
	for (int t = 0; t < platformNumber; t++)
	{
		for (int i = 0; i < deviceNumber; i++)
		{
			clReleaseKernel(platformy[t].platform_device[i].kernel);
			free(platformy[t].platform_device[i].bufforOUT);
			clReleaseCommandQueue(platformy[t].platform_device[i].queue);
			clReleaseMemObject(platformy[t].platform_device[i].bufforOut);
			clReleaseMemObject(platformy[t].platform_device[i].bufforIn);
			clReleaseContext(platformy[t].platform_device[i].context);
			clReleaseProgram(platformy[t].platform_device[i].program);
			clReleaseDevice(platformy[t].deviceIds[i]);
		}
		free(platformy[t].platform_device);
		free(platformy[t].deviceIds);
	}
	free(platformy);

	// Press Enter, to quit application.
	getchar();

	return 0;
}