
struct device{
	int device_id;
	char name[1024];
	char vendor[1024];
	char version[1024];
	size_t max_work_group;
	cl_context context;
	cl_kernel kernel;
	cl_command_queue queue;
	cl_program program;
	int bufforSize;
	cl_ulong maxDeviceMemSize;
	cl_mem bufforIn;
	cl_mem bufforOut;
	double *bufforOUT;
};

struct platforma{
	int platform_id;
	char name[1024];
	char vendor[1024];
	int device_number;
	cl_device_id* deviceIds;
	device *platform_device;
};

